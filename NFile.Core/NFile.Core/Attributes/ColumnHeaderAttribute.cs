﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class ColumnHeaderAttribute : Attribute
    {
        private string name;
        public string Name
        {
            get { return this.name; }
            set { name = value; }
        }

        public ColumnHeaderAttribute(string name)
        {
            this.name = name;
        }
    }
}
