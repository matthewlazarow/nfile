﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class OutputSaveAttribute : Attribute
    {
        public bool Save { get; set; } = true;
        public string HeaderName { get; set; }        
    }
}
