﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public class CellData : DynamicObject
    {
        private IDictionary<string, object> data = new Dictionary<string, object>();
 
        public object this[string key]
        {
            get
            {
                if (data.ContainsKey(key))
                {
                    return data[key];
                }
                throw new ArgumentException($"Column {key} does not exist.");
            }
            set
            {
                if (data.ContainsKey(key))
                {
                    throw new ArgumentException($"Column {key} has already been defined.");
                }
                data.Add(key, value);
            }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = this[binder.Name];            
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this[binder.Name] = value;
            return true;
        }        
    }
}
