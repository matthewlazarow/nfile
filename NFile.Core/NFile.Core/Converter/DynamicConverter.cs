﻿using System.Collections.Generic;

namespace NFile.Core
{
    public class DynamicConverter : IConverter
    {
        IParser _parser;
        IMapper _mapper;

        public DynamicConverter(IParser parser, IMapper mapper)
        {
            _parser = parser;
            _mapper = mapper;
        }

        public IEnumerable<T> Convert<T>()
        {
            return Convert<T>(_parser, _mapper);
        }

        public static IEnumerable<T> Convert<T>(IParser parser, IMapper mapper)
        {
            while (!parser.EndOfStream)
            {
                yield return InflateObject(parser.ReadRow(), mapper);
            }
        }

        private static dynamic InflateObject(string[] data, IMapper mapper)
        {
            var obj = new CellData();
            foreach (var map in mapper.Mappings)
            {
                if (map.ColumnIndex < data.Length)
                    obj[map.ColumnName] = data[map.ColumnIndex];
                else
                    obj[map.ColumnName] = null;
            }
                        
            return obj;
        }
    }
}
