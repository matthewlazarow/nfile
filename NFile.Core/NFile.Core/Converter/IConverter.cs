﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public interface IConverter
    {
        IEnumerable<T> Convert<T>();
    }
}
