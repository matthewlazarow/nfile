﻿using NFile.Core.Attributes;
using NFile.Core.PostProcessing;
using NFile.Core.PreProcessing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public class TextConverter : IConverter
    {
        IParser _parser;
        IMapper _mapper;

        public TextConverter(IParser parser, IMapper mapper)
        {
            _parser = parser;
            _mapper = mapper;
        }

        public IEnumerable<T> Convert<T>()
        {
            return Convert<T>(_parser, _mapper, null, null);
        }

        public IEnumerable<T> Convert<T>(IPreProcessor preProcessor, IPostProcessor<T> postProcessor)
        {
            return Convert<T>(_parser, _mapper, preProcessor, postProcessor);
        }

        public static IEnumerable<T> Convert<T>(IParser parser, IMapper mapper, IPreProcessor preProcessor, IPostProcessor<T> postProcessor)
        {
            var propCache = GetPropertyCache<T>(mapper);

            while (!parser.EndOfStream)
            {
                var dataRow = preProcessor == null ? parser.ReadRow() : preProcessor.Process(parser.ReadRow());
                var instance = InflateObject<T>(dataRow, propCache);
                yield return postProcessor == null ? instance : postProcessor.Process(instance);
            }
        }

        private static T InflateObject<T>(object[] data, Dictionary<PropertyInfo, int> mappedProps)
        {   
            var obj = Activator.CreateInstance<T>();
            foreach (var prop in mappedProps)         
            {
                var converter = TypeDescriptor.GetConverter(prop.Key.PropertyType);
                if (converter.CanConvertFrom(typeof(string)))
                {
                    var converted = converter.ConvertFrom(data[prop.Value]);
                    obj.GetType().GetProperty(prop.Key.Name).SetValue(obj, converted);
                }
            }
            
            return obj;
        }

        private static Dictionary<PropertyInfo, int> GetPropertyCache<T>(IMapper mapper)
        {
            Dictionary<PropertyInfo, int> mappedProps = new Dictionary<PropertyInfo, int>();
            var properties = typeof(T).GetProperties();
            var totalMappings = mapper.Mappings.Count();
            for (int i = 0; i < totalMappings; i++)
            {
                var prop = properties.Where(p => PropertyFilter(p, mapper[i])).SingleOrDefault();
                if (prop != null)
                {
                    mappedProps.Add(prop, i);
                }
            }
            return mappedProps;
        }
       
        private static bool PropertyFilter(PropertyInfo pi, IMapping mapping)
        {
            var headerAttribute = pi.GetCustomAttributes(true)
                .Where(a => a.GetType() == typeof(ColumnHeaderAttribute))
                .SingleOrDefault() as ColumnHeaderAttribute;
            if (headerAttribute?.Name == mapping?.ColumnName)
                return true;

            return pi.Name.Equals(mapping?.ColumnName, StringComparison.CurrentCultureIgnoreCase);
        }
        
    }
}
