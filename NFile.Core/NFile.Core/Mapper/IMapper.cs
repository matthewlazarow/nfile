﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public interface IMapper
    {
        IMapping this [string name] { get; }
        IMapping this[int index] { get; }

        int? FindColumnIndex(string columnName);
        string FindColumnName(int index);
        IMapping GetMapping(string columnName);
        IEnumerable<IMapping> Mappings { get; }
    }
}
