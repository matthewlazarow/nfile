﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public interface IMapping
    {
        string ColumnName { get; set; }
        string[] Aliases { get; set; }
        int ColumnIndex { get; set; }
        Type DataType { get; set; }
    }
}
