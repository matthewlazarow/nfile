﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    class Mapping : IMapping
    {
        public Mapping(string columnName, int columnIndex)
            :this(columnName, columnIndex, new string[0], null)
        {
            
        }

        public Mapping(string columnName, int columnIndex, string[] aliases)
            :this(columnName, columnIndex, aliases, null)
        {

        }
        public Mapping(string columnName, int columnIndex, string[] aliases, Type dataType)
        {
            ColumnIndex = columnIndex;
            ColumnName = columnName;
            Aliases = aliases;
            DataType = dataType;
        }

        public int ColumnIndex { get; set; }        
        public string ColumnName { get; set; }
        public string[] Aliases { get; set; }
        public Type DataType { get; set; }        
    }
}
