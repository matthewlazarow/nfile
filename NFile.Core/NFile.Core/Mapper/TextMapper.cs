﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public class TextMapper : IMapper
    {
        private IList<IMapping> ColumnMap;

        public TextMapper AddMapping(string name, int index)
        {
            if (FindColumnName(index) != null)            
                throw new ArgumentException($"Mapping index {index} already mapped to property name: {FindColumnName(index)}");
                        
            if (FindColumnIndex(name) != null)
                throw new ArgumentException($"Mapping property name {name} already mapped to index: {FindColumnIndex(name)}");

            ColumnMap.Add(new Mapping(name, index));
            return this;
        }

        public IMapping this [string value] 
        {
            get
            {
                return GetMapping(value);
            }
        }

        public IMapping this [int index]
        {
            get
            {
                var name = FindColumnName(index);                
                return GetMapping(name);
            }
        }

        public TextMapper()
        {
            ColumnMap = new List<IMapping>();
        }

        public TextMapper(string[] header)
        {
            ColumnMap = new List<IMapping>();
            for (var i = 0; i < header.Length; i++)
            {
                if (this[header[i]] != null)
                    throw new ArgumentException($"Header {header[i]} found in multiple columns: {this[header[i]].ColumnIndex} and {i}");

                ColumnMap.Add(new Mapping(header[i], i));
            }
        }

        public int? FindColumnIndex(string columnName)
        {
            return FindColumnIndex(columnName, StringComparison.CurrentCulture);
        }

        public int? FindColumnIndex(string columnName, StringComparison stringComparison)
        {
            return GetMapping(columnName, stringComparison)?.ColumnIndex;
        }

        public string FindColumnName(int index)
        {
            return ColumnMap.Where(c => c.ColumnIndex == index).SingleOrDefault()?.ColumnName;
        }

        public IMapping GetMapping(string columnName)
        {
            return GetMapping(columnName, StringComparison.CurrentCulture);
        }

        public IMapping GetMapping(string columnName, StringComparison stringComparison)
        {
            return ColumnMap.Where(c => c.ColumnName.Equals(columnName, stringComparison)).SingleOrDefault();
        }

        public IEnumerable<IMapping> Mappings
        {
            get
            {
                foreach (var map in ColumnMap)
                {
                    yield return map;
                }
            }
        }
    }
}
