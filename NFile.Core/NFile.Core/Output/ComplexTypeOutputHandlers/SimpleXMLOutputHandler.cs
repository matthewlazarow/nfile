﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace NFile.Core.Output
{
    public class SimpleXMLOutputHandler : IComplexTypeOutputHandler
    {
        private const string ELEMENTNAME = "object";

        public string Serialize(object input)
        {
            var buffer = new MemoryStream();           
            var xml = ToXElementInternal(input, null);
            return xml.ToString();
        }

        //checkout https://axelzarate.wordpress.com/2014/08/19/using-xslt-as-email-template-engine-in-c/

        private XElement SerializeEnumerable(IEnumerable input, string element)
        {
            var rootElement = new XElement(element);

            foreach (var value in input) { 
                XElement childElement = IsSimpleOrNullableType(value.GetType()) ? new XElement(element + "Item", value) : ToXElementInternal(value, element + "Item");

                rootElement.Add(childElement);
            }

            return rootElement;
        }

        private XElement ToXElementInternal(object input, string element)
        {
            if (input == null)
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(element))
            {
                element = ELEMENTNAME;
            }

            element = XmlConvert.EncodeName(element);

            var enumerable = input as IEnumerable;
            if (enumerable != null)
            {
                return SerializeEnumerable(enumerable, element);
            }

            var xElement = new XElement(element);

            var props = GetProperties(input);
            var elements = props
                .Where(x => x.Value != null)
                .Select(x => IsSimpleOrNullableType(x.Value.GetType())
                                ? new XElement(x.Key, x.Value)
                                : ToXElementInternal(x.Value, x.Key));

            xElement.Add(elements);

            return xElement;
        }

        private static IEnumerable<KeyValuePair<string, object>> GetProperties(object input)
        {
            var type = input.GetType();

            return type.GetProperties(BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public)
                .ToDictionary(x => x.Name, x => x.GetValue(input, null));
        }

        private static bool IsSimpleOrNullableType(Type type)
        {
            if (type == null) throw new ArgumentNullException("type");
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                type = Nullable.GetUnderlyingType(type);
            }

            return IsSimpleType(type);
        }

        private static bool IsSimpleType(Type type)
        {
            if (type == null) throw new ArgumentNullException("type");
            return type.IsPrimitive || type.IsEnum || type == typeof(string) || type == typeof(Decimal) || type == typeof(DateTime) || type == typeof(Guid);
        }
    }
}
