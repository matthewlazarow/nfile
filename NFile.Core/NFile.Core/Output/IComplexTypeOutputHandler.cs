﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Output
{
    public interface IComplexTypeOutputHandler
    {
        string Serialize(object input);
    }
}
