﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Output
{
    public interface IOutput
    {
        void Save<T>(T Record);
        void SaveMany<T>(IEnumerable<T> Records);
    }
}
