﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Output
{
    public interface IOutputSettings
    {
        IDelimiter Delimiter { get; set; }
        ITextQualifier TextQualifier { get; set; }
        Encoding OutputEncoding { get; set; }
        IComplexTypeOutputHandler ComplexTypeOutputHandler { get; set; }
    }
}
