﻿using NFile.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Output
{
    public abstract class OutputBase : IOutput
    {
        public abstract void Save<T>(T Record);
        public abstract void SaveMany<T>(IEnumerable<T> Records);

        public IEnumerable<object> GetObjectHeaders<T>(T obj)
        {
            foreach (PropertyInfo i in obj.GetType().GetProperties().Where(p => PropertyFilter(p)))
                yield return GetPropertyCustomHeaderName(i) ?? i.Name;
        }

        public IEnumerable<object> GetObjectData<T>(T obj)
        {
            foreach (PropertyInfo i in obj.GetType().GetProperties().Where(p => PropertyFilter(p)))
            {
                object v = i.GetValue(obj);
                if (v != null)
                    yield return v;
                else
                    yield return default(string);
            }
        }

        private static string GetPropertyCustomHeaderName(PropertyInfo pi)
        {
            return GetOutputAttribute(pi)?.HeaderName;
        }

        private static bool PropertyFilter(PropertyInfo pi)
        {            
            return GetOutputAttribute(pi)?.Save ?? true;
        }

        private static OutputSaveAttribute GetOutputAttribute(PropertyInfo pi)
        {
            return pi.GetCustomAttributes(true)
                .Where(a => a.GetType() == typeof(OutputSaveAttribute))
                .SingleOrDefault() as OutputSaveAttribute;
        }
    }
}
