﻿using NFile.Core.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NFile.Core.Output
{
    public class TextOutput : OutputBase
    {
        public TextOutputSettings Settings { get; set; }
        private Func<Stream> _stream;     

        public TextOutput(FileInfo file, TextOutputSettings settings) 
            :this(settings, file.OpenWrite)
        {

        }

        public TextOutput(TextOutputSettings configuration, Func<Stream> oStream)
        {
            _stream = oStream;
            Settings = configuration;                        
        }

        public override void Save<T>(T record)
        {
            SaveMany<T>(Enumerable.Repeat<T>(record, 1));
        }

        public override void SaveMany<T>(IEnumerable<T> records)
        {
            if (!records.Any()) return;

            string header = ReadHeader(records.First());
                
            using (var stream = _stream())
            using (StreamWriter fs = new StreamWriter(stream))
            {
                if (Settings.WriteHeaders)
                    Write(fs, header);

                foreach (var record in records)
                {
                    string objectDataString = ReadObjectData(record);
                    Write(fs, objectDataString); 
                    // TODO: Figure out how to handle newline when no headers are written. 
                    // appending it to end creates extra line at the end of the file on write. maybe this is ok?
                }
            }
        }

        private void Write(StreamWriter w, string data)
        {
            w.Write($"{data}{Environment.NewLine}");
        }

        private string ReadHeader<T>(T firstRow)
        {
            return string.Join(
                    Settings.Delimiter.Value,
                    GetObjectHeaders(firstRow)
                        .Select(h => Settings.TextQualifier.OpeningQualifier + EscapeQualifier(h.ToString(), Settings.TextQualifier) + Settings.TextQualifier.ClosingQualifier));
        }

        private string ReadObjectData<T>(T record)
        {
            return string.Join(Settings.Delimiter.Value, GetObjectData(record)
                .Select(v =>
                    Settings.TextQualifier.OpeningQualifier +
                    EscapeQualifier(ProcessValue(v), Settings.TextQualifier) + Settings.TextQualifier.ClosingQualifier));
        }

        private string ProcessValue(object value)
        {
            if (value == null) { return null; }
            Type t = value.GetType();
            if (t.Module.ScopeName == "CommonLanguageRuntimeLibrary" && t.Namespace != null
                && !t.IsArray && !t.IsGenericType)
            {
                return value.ToString();
            }
            else
            {
                return Settings.ComplexTypeOutputHandler.Serialize(value);
            }
        }

        private string EscapeQualifier(string data, ITextQualifier qualifier)
        {
            return qualifier.EscapeClosingQualifier(qualifier.EscapeOpeningQualifier(data ?? ""));           
        }        
    }
}
