﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.Output
{
    public class TextOutputSettings : IOutputSettings
    {
        public IComplexTypeOutputHandler ComplexTypeOutputHandler { get; set; }
        public IDelimiter Delimiter { get; set; }
        public Encoding OutputEncoding { get; set; }
        public ITextQualifier TextQualifier { get; set; }
        public bool WriteHeaders { get; set; } = true;

        public TextOutputSettings() { }
        public TextOutputSettings(Encoding encoding, IDelimiter delimiter, ITextQualifier textQualifier, IComplexTypeOutputHandler complexOutputHandler, bool writeHeaders = true)
        {
            OutputEncoding = encoding;
            Delimiter = delimiter;
            TextQualifier = textQualifier;
            ComplexTypeOutputHandler = complexOutputHandler;
            WriteHeaders = writeHeaders;
        }
    }
}
