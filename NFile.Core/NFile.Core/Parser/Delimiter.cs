﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    /// <summary>
    /// Delimiter helper for most common values. Implement ITextQualifier to create custom implementations.
    /// </summary>
    public class Delimiter : IDelimiter
    {
        public static Delimiter Comma { get { return new Delimiter(","); } }
        public static Delimiter Pipe { get { return new Delimiter("|"); } }
        public static Delimiter Tab { get { return new Delimiter("\t"); } }

        public string Value { get; set; }
       
        public Delimiter(string delimiter)
        {
            Value = delimiter;
        }        
    }
}
