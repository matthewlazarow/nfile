﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public interface ITextQualifier
    {
        string OpeningQualifier { get; set; }
        string ClosingQualifier { get; set; }
        Func<string, string> EscapeOpeningQualifier { get; set; }
        Func<string, string> EscapeClosingQualifier { get; set; }
    }
}
