﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    internal class TextDataChunk
    {
        internal List<string> Data { get; set; }
        internal bool Continuation { get; set; }
        internal void KeepReading(TextDataChunk nextLine)
        {
            var stitched = Data.Last() + nextLine.Data.First();
            var list1 = Data;
            var list2 = nextLine.Data; ;

            Data.RemoveAt(list1.Count - 1);
            nextLine.Data.RemoveAt(0);

            Data.Add(stitched);
            Data.AddRange(nextLine.Data);

            Continuation = nextLine.Continuation;
        }
    }
}
