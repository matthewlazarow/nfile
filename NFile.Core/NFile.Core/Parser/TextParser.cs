﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    public class TextParser : IParser, IDisposable
    {
        public IDelimiter Delimeter { get; set; }
        public ITextQualifier TextQualifier { get; set; }
        public Encoding Encoding { get; set; }
        public string[] Header { get; private set; }
        private Stream _stream;
        private StreamReader _reader;        

        public TextParser(Stream stream, IDelimiter delimiter, ITextQualifier textQualifier)
            :this(stream, delimiter, textQualifier, Encoding.Default, true)
        {

        }

        public TextParser(Stream stream, IDelimiter delimiter, ITextQualifier textQualifier, Encoding encoding, bool containsHeader)
        {
            Delimeter = delimiter;
            TextQualifier = textQualifier;
            Encoding = encoding;
            _stream = stream;
            _reader = new StreamReader(_stream);
            if (containsHeader)
            {
                Header = this.ReadRow();
            }
        }

        public bool EndOfStream
        {
            get { return _reader.EndOfStream; }
        }

        public string[] ReadRow()
        {
            if (string.IsNullOrEmpty(Delimeter.Value))
                throw new ArgumentNullException("No Delimeter Specified.");

            if (_stream == null || !_stream.CanRead)
                throw new ArgumentException("No or Invalid Stream.");
                                                
            if (!_reader.EndOfStream)
            {
                var rowData = ParseRow(_reader.ReadLine());
                while (rowData.Continuation)
                {
                    rowData.KeepReading(ParseRow(_reader.ReadLine(), rowData.Continuation));
                }
                return rowData.Data.ToArray();
            }
            return null;           
        }

        public IEnumerable<string[]> ReadRows()
        {
            while (!_reader.EndOfStream)
            {
                yield return ReadRow();
            }
        }

        internal TextDataChunk ParseRow(string row, bool continuation = false)
        {
            if (TextQualifier.OpeningQualifier == "")
            {
                return new TextDataChunk
                {
                    Data = row.Split(new string[] { Delimeter.Value }, StringSplitOptions.None).ToList(),
                    Continuation = false
                };
            }

            var dataChunk = new TextDataChunk();
            List<string> tempData = new List<string>();
            List<char> buffer = new List<char>();
            bool insideTextQualifier = continuation;
            foreach (var c in row)
            {                
                if (insideTextQualifier && c == TextQualifier.ClosingQualifier[0])
                {
                    insideTextQualifier = false;
                    continue;
                }

                if (c == TextQualifier.OpeningQualifier[0])
                {
                    insideTextQualifier = true;
                    continue;
                }

                if (c == Delimeter.Value[0] && !insideTextQualifier)
                {
                    tempData.Add(new string(buffer.ToArray()));
                    buffer.Clear();
                    continue;
                }

                buffer.Add(c);
            }

            tempData.Add(new string(buffer.ToArray()));

            dataChunk.Continuation = insideTextQualifier; //if we end but haven't closed out it means the data continues on the next line...
            dataChunk.Data = tempData;
            return dataChunk;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _stream.Flush();
                    _stream.Close();
                    _stream.Dispose();

                    _reader.Close();
                    _reader.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

    }
}
