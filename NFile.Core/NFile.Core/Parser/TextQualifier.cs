﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core
{
    /// <summary>
    /// TextQualifier helper for most common values. Implement ITextQualifier to create custom implementations.
    /// </summary>
    public class TextQualifier : ITextQualifier
    {
        public static TextQualifier None { get { return new TextQualifier(""); } }
        public static TextQualifier Quote { get { return new TextQualifier("'", (s) => s.Replace("'", "''")); } }
        public static TextQualifier DoubleQuote { get { return new TextQualifier("\"", (s) => s.Replace("\"", "\"\"")); } }
        public static TextQualifier Tilde { get { return new TextQualifier("~"); } }

        public string OpeningQualifier { get; set; }
        public string ClosingQualifier { get; set; }
        public Func<string, string> EscapeOpeningQualifier { get; set; }
        public Func<string, string> EscapeClosingQualifier { get; set; }

        public TextQualifier(string qualifier)
            :this(qualifier, qualifier)
        {   
                     
        }

        public TextQualifier(string qualifier, Func<string, string> escapeQualifier)
            :this(qualifier, qualifier, escapeQualifier, (s) => s)
        {

        }

        public TextQualifier(string openingQualifier, string closingQualifier)
            :this(openingQualifier, closingQualifier, (s) => s, (s) => s)
        {
                        
        }
        
        public TextQualifier(string openingQualifier, string closingQualifier, Func<string, string> escapeOpeningQualifier, Func<string, string> escapeClosingQualifier)
        {
            OpeningQualifier = openingQualifier;
            ClosingQualifier = closingQualifier;
            EscapeOpeningQualifier = escapeOpeningQualifier;
            EscapeClosingQualifier = escapeClosingQualifier;
        }        
    }
}
