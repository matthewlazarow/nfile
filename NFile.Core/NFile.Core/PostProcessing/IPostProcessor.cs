﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.PostProcessing
{
    public interface IPostProcessor<T>
    {
        T Process(T entity);
    }
}
