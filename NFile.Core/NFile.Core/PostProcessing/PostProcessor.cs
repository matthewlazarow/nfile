﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.PostProcessing
{
    public class PostProcessor<T> : IPostProcessor<T>
    {
        private IList<Func<T, T>> functions;

        public PostProcessor()
        {
            functions = new List<Func<T, T>>();    
        }

        public void AddFunction(Func<T, T> function)
        {
            functions.Add(function);
        }
        
        public T Process(T entity)
        {
            T data = entity;
            foreach (var function in functions)
            {
                data = function(data);
            }

            return data;
        }
    }
}
