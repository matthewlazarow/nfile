﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.PreProcessing
{
    public interface IPreProcessor
    {
        object[] Process(string[] data);
    }
}
