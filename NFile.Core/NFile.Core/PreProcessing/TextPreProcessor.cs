﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Core.PreProcessing
{
    public class TextPreProcessor : IPreProcessor
    {
        private Dictionary<IMapping, Func<string, object>> functions;
        private IMapper mappings;

        public TextPreProcessor(IMapper preProcessorMapper)
            :this(preProcessorMapper, new Dictionary<IMapping, Func<string, object>>())
        {
           
        }

        public TextPreProcessor(IMapper preProcessorMapper, Dictionary<IMapping, Func<string, object>> preProcessorFunctions)
        {
            mappings = preProcessorMapper;
            functions = preProcessorFunctions;
        }

        public void AddFunction(string column, Func<string,object> function)
        {
            functions.Add(mappings.GetMapping(column), function);
        }
        
        public object[] Process(string[] data)
        {
            foreach (var function in functions)
            {
                var index = mappings.FindColumnIndex(function.Key.ColumnName);
                if (index.HasValue)
                {
                    var sourceData = data[index.Value];
                    data[index.Value] = function.Value(sourceData).ToString();
                }
            }

            return data;
        }
    }
}
