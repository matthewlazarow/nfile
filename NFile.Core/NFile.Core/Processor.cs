﻿using NFile.Core.Output;
using NFile.Core.PostProcessing;
using NFile.Core.PreProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NFile.Core
{
    public static class Processor
    {
        #region File_To_Objects        
        public static IEnumerable<T> GetObjects<T>(string file, string delimiter, string textQualifier, Encoding encoding, IPreProcessor preProcessor = null, IPostProcessor<T> postProcessor = null)            
        {
            return GetObjects<T>(file, new Delimiter(delimiter), new TextQualifier(textQualifier), encoding, preProcessor, postProcessor);
        }

        public static IEnumerable<T> GetObjects<T>(string file, IDelimiter delimiter, ITextQualifier textQualifier, Encoding encoding, IPreProcessor preProcessor = null, IPostProcessor<T> postProcessor = null)
        {
            var sourceFile = new FileInfo(file);
            using (var stream = sourceFile.OpenRead())
            {
                var objects = GetObjects<T>(stream, delimiter, textQualifier, encoding, preProcessor, postProcessor);
                foreach (var obj in objects)
                {
                    yield return obj;
                }
            }                        
        }

        public static IEnumerable<T> GetObjects<T>(Stream stream, IDelimiter delimiter, ITextQualifier textQualifier, Encoding encoding, IPreProcessor preProcessor = null, IPostProcessor<T> postProcessor = null)
        {
            var parser = new TextParser(stream, delimiter, textQualifier, Encoding.Default, true);
            var mapper = new TextMapper(parser.Header);
            var objects = GetObjects<T>(parser, mapper, preProcessor, postProcessor);
            foreach (var obj in objects)
            {
                yield return obj;
            }
        }

        public static IEnumerable<T> GetObjects<T>(IParser parser, IMapper mapper, IPreProcessor preProcessor = null, IPostProcessor<T> postProcessor = null)
        {
            return new TextConverter(parser, mapper).Convert<T>(preProcessor, postProcessor);
        }   
        
        public static IEnumerable<dynamic> GetDynamicObjects(string file, string delimiter, string textQualifier, Encoding encoding)
        {
            return GetDynamicObjects(file, new Delimiter(delimiter), new TextQualifier(textQualifier), encoding);
        }

        public static IEnumerable<dynamic> GetDynamicObjects(string file, IDelimiter delimiter, ITextQualifier textQualifier, Encoding encoding)
        {
            var sourceFile = new FileInfo(file);
            using (var stream = sourceFile.OpenRead())
            {
                foreach (var obj in GetDynamicObjects(stream, delimiter, textQualifier, encoding))
                {
                    yield return obj;
                }
            }
        }

        public static IEnumerable<dynamic> GetDynamicObjects(Stream stream, IDelimiter delimiter, ITextQualifier textQualifier, Encoding encoding)
        {
            var parser = new TextParser(stream, delimiter, textQualifier, Encoding.Default, true);
            var mapper = new TextMapper(parser.Header);
            var objects = new DynamicConverter(parser, mapper).Convert<dynamic>();
            foreach (var obj in objects)
            {
                yield return obj;
            }
        }

        public static IEnumerable<dynamic> GetDynamicObjects(IParser parser, IMapper mapper)
        {
            return new DynamicConverter(parser, mapper).Convert<dynamic>();
        }

        #endregion
        #region Objects_To_File

        public static void CreateOrAppendToFile<T>(IEnumerable<T> data, string file, IDelimiter delimiter, ITextQualifier textQualifier)
        {
            if (File.Exists(file))
            {
                AppendToFile<T>(data, file, delimiter, textQualifier);
            }
            else
            {
                SaveObjects<T>(data, file, delimiter, textQualifier);
            }
        }

        public static void AppendToFile<T>(IEnumerable<T> data, string file, IDelimiter delimiter, ITextQualifier textQualifier)
        {
            var configuration = new TextOutputSettings(Encoding.Default, delimiter, textQualifier, new SimpleXMLOutputHandler(), false);
            SaveObjects<T>(data, new TextOutput(configuration, () => File.Open(file, FileMode.Append)));
        }

        public static void SaveObjects<T>(IEnumerable<T> data, string file, string delimiter, string textQualifier)
        {
            if (File.Exists(file))
            {
                throw new ArgumentException($"File {file} already exists.");
            }

            SaveObjects<T>(data, file, new Delimiter(delimiter), new TextQualifier(textQualifier));
        }

        public static void SaveObjects<T>(IEnumerable<T> data, string file, IDelimiter delimiter, ITextQualifier textQualifier)
        {
            if (File.Exists(file))
            {
                throw new ArgumentException($"File {file} already exists.");
            }

            var configuration = new TextOutputSettings(Encoding.Default, delimiter, textQualifier, new SimpleXMLOutputHandler());
            SaveObjects<T>(data, new TextOutput(new FileInfo(file), configuration));
        }
        
        public static void SaveObjects<T>(IEnumerable<T> data, IOutput output)
        {
            output.SaveMany<T>(data);
        }

        public static void SaveObject<T>(T record, IOutput output)
        {
            output.Save(record);
        }

        #endregion
    }
}
