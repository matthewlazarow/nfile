﻿using NUnit.Framework;
using NFile.Core;
using NFile.Core.Attributes;
using System.Linq;
using System.Text;
using NFile.Testing.TestClasses;

namespace NFile.Testing.Load
{
    [TestFixture]
    public class load_Processor
    {
        [Test, MaxTime(20000)]
        public void LoadTest_Headers_100k()
        {
            //This database is copyright 1996 - 2017 by Sean Lahman.
            //This work is licensed under a Creative Commons Attribution - ShareAlike 3.0 Unported License.  For details see: http://creativecommons.org/licenses/by-sa/3.0/
            //http://seanlahman.com/baseball-archive/statistics/
            var source = ResourceHelper.GetResource("Batting.csv");
            var records = Processor.GetObjects<BattingRecord>(source, Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default).ToList();

            Assert.AreEqual(102816, records.Count);

        }       
    }
}
