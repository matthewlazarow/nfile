﻿using NFile.Core.Output;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing
{
    internal class MemoryOutput : OutputBase
    {
        public string Results { get; private set; }

        public override void Save<T>(T Record)
        {
            throw new NotImplementedException();
        }

        public override void SaveMany<T>(IEnumerable<T> Records)
        {
            string header = string.Join("|", GetObjectHeaders(Records.First()).ToList());

            using (var stream = new MemoryStream())
            using (StreamWriter fs = new StreamWriter(stream))
            {
                fs.WriteLine(header);
                foreach (var record in Records)
                {
                    var objectDataString = string.Join("|", GetObjectData(record)
                        .Select(v => "" + v + ""));
                    fs.WriteLine(objectDataString);
                }
                fs.Flush();
                Results = ASCIIEncoding.ASCII.GetString(stream.ToArray());
            }
        }
    }
}
