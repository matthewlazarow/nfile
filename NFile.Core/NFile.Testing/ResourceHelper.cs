﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing
{
    public static class ResourceHelper
    {
        public static Stream GetResource(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"NFile.Testing.SourceFiles.{name}";
            return assembly.GetManifestResourceStream(resourceName);
        }
    }
}
