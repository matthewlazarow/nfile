﻿using NFile.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing.TestClasses
{
    class BattingRecord
    {
        [ColumnHeader("playerID")]
        public string Id { get; set; }
        [ColumnHeader("yearID")]
        public int Year { get; set; }
        [ColumnHeader("stint")]
        public int Stint { get; set; }
        [ColumnHeader("teamID")]
        public string TeamId { get; set; }
        [ColumnHeader("lgID")]
        public string LGId { get; set; }
        [ColumnHeader("G")]
        public int G { get; set; }
        [ColumnHeader("AB")]
        public int AB { get; set; }
        [ColumnHeader("R")]
        public int R { get; set; }
        [ColumnHeader("H")]
        public int H { get; set; }
        [ColumnHeader("2B")]
        public int? Second { get; set; }
        [ColumnHeader("3B")]
        public int? Third { get; set; }
        [ColumnHeader("HR")]
        public int? HomeRun { get; set; }
        [ColumnHeader("RBI")]
        public int? RunBattedIn { get; set; }
        [ColumnHeader("SB")]
        public int? SB { get; set; }
        [ColumnHeader("CS")]
        public int? CS { get; set; }
        [ColumnHeader("BB")]
        public int? BB { get; set; }
        [ColumnHeader("SO")]
        public int? SO { get; set; }
        [ColumnHeader("IBB")]
        public int? IBB { get; set; }
        [ColumnHeader("HBP")]
        public int? HBP { get; set; }
        [ColumnHeader("SH")]
        public int? SH { get; set; }
        [ColumnHeader("SF")]
        public int? SF { get; set; }
        [ColumnHeader("GIDP")]
        public int? GIDP { get; set; }
    }
}
