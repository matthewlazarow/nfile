﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing.TestFixturees
{
    public class TypedTestFixture
    {
        public int IntColumn { get; set; }
        public DateTime? DateColumn { get; set; }
        public bool BoolColumn { get; set; }
        public string StringColumn { get; set; }
    }
}
