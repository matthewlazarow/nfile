﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NFile.Core.Attributes;

namespace NFile.Testing.TestFixturees
{
    public class TypedTestFixtureAttributes
    {
        [ColumnHeader("Total")]
        public int IntColumn { get; set; }

        [ColumnHeader("Date")]
        public DateTime? DateColumn { get; set; }

        [ColumnHeader("Active")]
        [OutputSave(HeaderName = "Is Currently Active")]
        public bool BoolColumn { get; set; }

        [ColumnHeader("Email")]
        [OutputSave(HeaderName = "Email Address")]
        public string StringColumn { get; set; }

        [ColumnHeader("Feet And Inches")]
        [OutputSave(HeaderName = "Total ' and \"")]
        public string Length { get; set; }

        [ColumnHeader("HiddenTest")]
        [OutputSave(Save = false)]
        public string Hidden { get; set; }
    }
}
