﻿using NFile.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing.TestFixturees
{
    public class TypedTestFixtureAttributesInherited : TypedTestFixtureAttributes
    {
        [ColumnHeader("Meta1")]
        public string ExtraInformation { get; set; }
    }
}
