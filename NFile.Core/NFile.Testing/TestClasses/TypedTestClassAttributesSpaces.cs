﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NFile.Core.Attributes;

namespace NFile.Testing.TestFixturees
{
    public class TypedTestFixtureAttributesSpaces
    {
        [ColumnHeader("Total Number")]
        public int IntColumn { get; set; }

        [ColumnHeader("Date Qualified")]
        public DateTime? DateColumn { get; set; }

        [ColumnHeader("Active Bit")]
        public bool BoolColumn { get; set; }

        [ColumnHeader("Email Address")]
        public string StringColumn { get; set; }

        public string Meta1 { get; set; }
    }
}
