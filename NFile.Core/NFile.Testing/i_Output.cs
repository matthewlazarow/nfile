﻿using NUnit.Framework;
using NFile.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing
{
    public class record
    {
        public string ColumnA { get; set; }
        public int ColumnB { get; set; }
        public DateTime ColumnC { get; set; }
    }

    [TestFixture]
    public class i_Output
    {
        List<record> _data = new List<record>
        {
            new record
            {
                ColumnA = "Data1A",
                ColumnB = 5,
                ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
            },
            new record
            {
                ColumnA = "Data2A",
                ColumnB = 10,
                ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
            }
        };
        
        [Test]
        public void Output_Text_TempFile_Create()
        {
            var file = Path.Combine(Path.GetTempPath(), "nfile-test.txt");
            if (File.Exists(file))
            {
                File.Delete(file);
            }
            Processor.SaveObjects(_data, file, Delimiter.Pipe, TextQualifier.None);

            Assert.IsTrue(File.Exists(file));

            var processed = Processor.GetObjects<record>(file, Delimiter.Pipe, TextQualifier.None, Encoding.Default, null, null);

            Assert.AreEqual(2, processed.Count());
            Assert.AreEqual("Data1A", processed.First().ColumnA);
            Assert.AreEqual(10, processed.Last().ColumnB);

            File.Delete(file);           
        }

        [Test]
        public void Output_Text_TempFile_Create_and_Append()
        {
            var file = Path.Combine(Path.GetTempPath(), "nfile-test.txt");
            
            for (var i = 0; i < 5; i++)
            {
                Processor.CreateOrAppendToFile(_data, file, Delimiter.Pipe, TextQualifier.None);
            }

            var processed = Processor.GetObjects<record>(file, Delimiter.Pipe, TextQualifier.None, Encoding.Default, null, null);

            Assert.AreEqual(10, processed.Count());
            Assert.AreEqual("Data1A", processed.First().ColumnA);
            Assert.AreEqual(10, processed.Last().ColumnB);

            File.Delete(file);
        }

        [Test]
        public void Output_Text_TempFile_Exists()
        {
            var file = Path.GetTempFileName();
            Assert.Throws<ArgumentException>(() => Processor.SaveObjects(_data, file, Delimiter.Pipe, TextQualifier.None));
            File.Delete(file);
        }
    }
}
