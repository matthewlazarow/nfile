﻿using System;
using NUnit.Framework;
using NFile.Testing.TestFixturees;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NFile.Core;
using System.IO;
using NFile.Core.PreProcessing;
using NFile.Core.PostProcessing;
using NFile.Core.Output;
using Microsoft.CSharp.RuntimeBinder;

namespace NFile.Testing
{
    [TestFixture]
    public class i_Processor
    {        
        [Test]
        public void Process_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TestFileClass>(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void Process_Headless_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipeHeadless.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None, Encoding.Default, false);
            var mapper = new TextMapper()
                .AddMapping("ColumnA", 0)
                .AddMapping("ColumnB", 1)
                .AddMapping("ColumnC", 2)
                .AddMapping("ColumnD", 3);
            var objects = Processor.GetObjects<TestFileClass>(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }
        
        [Test]
        public void Process_DoubleQuoteTextQualifier_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipeDoubleQuote.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.DoubleQuote);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TestFileClass>(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void Process_DoubleQuoteTextQualifierMultiLine_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipeDoubleQuoteMultiLine.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.DoubleQuote);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TestFileClass>(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void Process_DistinctTextQualifierTest_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipeDistinctQualifiers.dat");
            var parser = new TextParser(file, Delimiter.Pipe, new TextQualifier("{", "}"));
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TestFileClass>(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void Process_Get_Typed_Objects_Header_Attributes()
        {
            var file = ResourceHelper.GetResource("TypedTestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TypedTestFixtureAttributes>(parser, mapper).ToList();

            Assert.AreEqual(15, objects[0].IntColumn);
            Assert.AreEqual(DateTime.Parse("2016-08-29"), objects[0].DateColumn);
            Assert.AreEqual(true, objects[0].BoolColumn);
            Assert.AreEqual("noone@somewhere.com", objects[0].StringColumn);
        }

        [Test]
        public void Process_Get_Typed_Objects_Header_Attributes_Inherited()
        {
            var file = ResourceHelper.GetResource("TypedTestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TypedTestFixtureAttributesInherited>(parser, mapper).ToList();

            Assert.AreEqual(15, objects[0].IntColumn);
            Assert.AreEqual(DateTime.Parse("2016-08-29"), objects[0].DateColumn);
            Assert.AreEqual(true, objects[0].BoolColumn);
            Assert.AreEqual("noone@somewhere.com", objects[0].StringColumn);
            Assert.AreEqual("Extra Data", objects[0].ExtraInformation);
        }

        [Test]
        public void Process_Get_Typed_Objects_Header_Attributes_Spaces()
        {
            var file = ResourceHelper.GetResource("TypedTestFilePipeHeaderSpaces.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetObjects<TypedTestFixtureAttributesSpaces>(parser, mapper).ToList();

            Assert.AreEqual(15, objects[0].IntColumn);
            Assert.AreEqual(DateTime.Parse("2016-08-29"), objects[0].DateColumn);
            Assert.AreEqual(true, objects[0].BoolColumn);
            Assert.AreEqual("noone@somewhere.com", objects[0].StringColumn);
            Assert.AreEqual("Extra Data", objects[0].Meta1);          
        }

        [Test]
        public void Process_PreProcessor_Test()
        {
            var file = ResourceHelper.GetResource("BadTypedTestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var preProcessor = new TextPreProcessor(mapper);

            preProcessor.AddFunction("IntColumn", (s) =>
            {
                int result;
                if (int.TryParse(s, out result))
                {
                    return result;
                }
                return 0;
            });

            preProcessor.AddFunction("DateColumn", (s) =>
            {
                switch (s)
                {
                    case "Yesterday": return DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
                    case "Tomorrow": return DateTime.Now.AddDays(1.0);
                    default: return DateTime.Parse(s);
                }
            });

            preProcessor.AddFunction("BoolColumn", (s) =>
            {
                switch (s)
                {
                    case "Nope": return false;
                    case "Yep": return true;
                    default: return bool.Parse(s);
                }
            });

            var objects = Processor.GetObjects<TypedTestFixture>(parser, mapper, preProcessor).ToList();

            Assert.AreEqual(3, objects.Count);

            Assert.AreEqual(0, objects[0].IntColumn);
            Assert.AreEqual(DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).ToShortDateString(), objects[0].DateColumn.Value.ToShortDateString());
            Assert.AreEqual(false, objects[0].BoolColumn);
            Assert.AreEqual("Data1D", objects[0].StringColumn);

            Assert.AreEqual(0, objects[1].IntColumn);
            Assert.AreEqual(DateTime.Now.AddDays(1.0).ToShortDateString(), objects[1].DateColumn.Value.ToShortDateString());
            Assert.AreEqual(true, objects[1].BoolColumn);
            Assert.AreEqual("Data2D", objects[1].StringColumn);

            Assert.AreEqual(15, objects[2].IntColumn);
            Assert.AreEqual(DateTime.Parse("2016-08-29"), objects[2].DateColumn);
            Assert.AreEqual(true, objects[2].BoolColumn);
            Assert.AreEqual("Data3D", objects[2].StringColumn);

        }

        [Test]
        public void Process_PostProcessor_Test()
        {
            var file = ResourceHelper.GetResource("TestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var postProcessor = new PostProcessor<TestFileClass>();

            postProcessor.AddFunction((test) =>
            {
                if (test.ColumnB.Contains("2"))
                {
                    test.ColumnB = "Modified Value";
                    test.ColumnD = "Modified Value";
                }
                return test;
            });

            var objects = Processor.GetObjects<TestFileClass>(parser, mapper, null, postProcessor).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data1B", objects[0].ColumnB);
            Assert.AreEqual("Data1C", objects[0].ColumnC);
            Assert.AreEqual("Data1D", objects[0].ColumnD);
            Assert.AreEqual("Data2A", objects[1].ColumnA);
            Assert.AreEqual("Modified Value", objects[1].ColumnB);
            Assert.AreEqual("Data2C", objects[1].ColumnC);
            Assert.AreEqual("Modified Value", objects[1].ColumnD);
            Assert.AreEqual("Data3A", objects[2].ColumnA);
            Assert.AreEqual("Data3B", objects[2].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data3D", objects[2].ColumnD);
            Assert.AreEqual("Data4A", objects[3].ColumnA);
            Assert.AreEqual("Data4B", objects[3].ColumnB);
            Assert.AreEqual("Data4C", objects[3].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
            Assert.AreEqual("Data5B", objects[4].ColumnB);
            Assert.AreEqual("Data5C", objects[4].ColumnC);
            Assert.AreEqual("Data5D", objects[4].ColumnD);
        }        
    }   
}
