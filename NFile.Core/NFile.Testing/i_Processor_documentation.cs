﻿using System;
using NUnit.Framework;
using NFile.Testing.TestFixturees;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NFile.Core;
using System.IO;
using NFile.Core.PreProcessing;
using NFile.Core.PostProcessing;
using NFile.Core.Output;
using Microsoft.CSharp.RuntimeBinder;
using NFile.Core.Attributes;

namespace NFile.Testing
{
    [TestFixture]
    public class i_Processor_documentation
    {   
        private Stream _banklist
        {
            get
            {
                return ResourceHelper.GetResource("banklist.csv");
            }
        }

        private Stream _contacts
        {
            get
            {
                return ResourceHelper.GetResource("contacts.csv");
            }
        }

        public class BankRecord
        {
            [OutputSave(Save = false)]
            public Guid Id { get; set; }

            [ColumnHeader("Bank Name")]
            [OutputSave(HeaderName = "Institution")]
            public string Name { get; set; }

            public string City { get; set; }

            [ColumnHeader("ST")]
            public string State { get; set; }

            public string CERT { get; set; }

            [ColumnHeader("Acquiring Institution")]
            [OutputSave(HeaderName = "Purchasing Bank")]
            public string Buyer { get; set; }

            [ColumnHeader("Closing Date")]
            [OutputSave(HeaderName = "Terminated Date")]
            public DateTime ClosingDate { get; set; }

            [ColumnHeader("Updated Date")]
            [OutputSave(HeaderName = "Modified")]
            public DateTime Updated { get; set; }
        }

        [Test]
        public void Documentation_Dynamic()
        {
            var data = Processor.GetDynamicObjects(_banklist, Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default);
            
            var expected = new string[] {
                "Bank First CornerStone Bank from PA closed 6-May-16",
                "Bank Vantage Point Bank from PA closed 28-Feb-14",
                "Bank NOVA Bank from PA closed 26-Oct-12",
                "Bank American Eagle Savings Bank from PA closed 20-Jan-12",
                "Bank Public Savings Bank from PA closed 18-Aug-11",
                "Bank Earthstar Bank from PA closed 10-Dec-10",
                "Bank Allegiance Bank of North America from PA closed 19-Nov-10",
                "Bank Dwelling House Savings and Loan Association from PA closed 14-Aug-09",
                "Bank Metropolitan Savings Bank from PA closed 2-Feb-07",
                "Bank Pulaski Savings Bank from PA closed 14-Nov-03"
            };

            int index = 0;
            foreach (var record in data.Where(d => d.ST == "PA"))
            {
                var computed = $"Bank {record["Bank Name"]} from {record["ST"]} closed {record["Closing Date"]}";
                Assert.AreEqual(expected[index++], computed);
            }
        }

        [Test]
        public void Documentation_TypedClass()
        {
            var data = Processor.GetObjects<BankRecord>(_banklist, Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default);

            var expected = new string[] {
                "Bank First CornerStone Bank from PA closed 5/6/2016 12:00:00 AM",
                "Bank Vantage Point Bank from PA closed 2/28/2014 12:00:00 AM",
                "Bank NOVA Bank from PA closed 10/26/2012 12:00:00 AM",
                "Bank American Eagle Savings Bank from PA closed 1/20/2012 12:00:00 AM",
                "Bank Public Savings Bank from PA closed 8/18/2011 12:00:00 AM",
                "Bank Earthstar Bank from PA closed 12/10/2010 12:00:00 AM",
                "Bank Allegiance Bank of North America from PA closed 11/19/2010 12:00:00 AM",
                "Bank Dwelling House Savings and Loan Association from PA closed 8/14/2009 12:00:00 AM",
                "Bank Metropolitan Savings Bank from PA closed 2/2/2007 12:00:00 AM",
                "Bank Pulaski Savings Bank from PA closed 11/14/2003 12:00:00 AM"
            };

            int index = 0;
            foreach (var record in data.Where(d => d.State == "PA"))
            {
                var computed = $"Bank {record.Name} from {record.State} closed {record.ClosingDate}";
                Assert.AreEqual(expected[index++], computed);
            }
        }
        
        public class Contact
        {
            public string Email { get; set; }

            [ColumnHeader("First Name")]
            public string First { get; set; }

            [ColumnHeader("Last Name")]
            public string Last { get; set; }

            public string Phone { get; set; }
        }

        [Test]
        public void Documentation_ManualMappings()
        {
            using (var stream = _contacts)
            {
                var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.None, Encoding.Default, false);
                var mapper = new TextMapper()
                        .AddMapping("Email", 0)
                        .AddMapping("First Name", 1)
                        .AddMapping("Last Name", 2)
                        .AddMapping("Phone", 3);
                var objects = Processor.GetObjects<Contact>(parser, mapper).ToList();

                Assert.AreEqual(2, objects.Count);
                Assert.AreEqual("testuser@somewhere.com", objects[0].Email);
                Assert.AreEqual("Tom", objects[0].First);
                Assert.AreEqual("TheMouse", objects[1].Last);
                Assert.AreEqual("555-5556", objects[1].Phone);
            }
        }

        [Test]
        public void Documentation_PreProcessor()
        {
            var expected = new string[] {
                "Bank First CornerStone Bank from Pennsylvania closed 5/6/2016 12:00:00 AM",
                "Bank Vantage Point Bank from Pennsylvania closed 2/28/2014 12:00:00 AM",
                "Bank NOVA Bank from Pennsylvania closed 10/26/2012 12:00:00 AM",
                "Bank American Eagle Savings Bank from Pennsylvania closed 1/20/2012 12:00:00 AM",
                "Bank Public Savings Bank from Pennsylvania closed 8/18/2011 12:00:00 AM",
                "Bank Earthstar Bank from Pennsylvania closed 12/10/2010 12:00:00 AM",
                "Bank Allegiance Bank of North America from Pennsylvania closed 11/19/2010 12:00:00 AM",
                "Bank Dwelling House Savings and Loan Association from Pennsylvania closed 8/14/2009 12:00:00 AM",
                "Bank Metropolitan Savings Bank from Pennsylvania closed 2/2/2007 12:00:00 AM",
                "Bank Pulaski Savings Bank from Pennsylvania closed 11/14/2003 12:00:00 AM"
            };

            using (var stream = _banklist)
            {
                var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote);
                var mapper = new TextMapper(parser.Header);
                var preProcessor = new TextPreProcessor(mapper);

                preProcessor.AddFunction("ST", (s) =>
                {
                    if (s.Equals("PA", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return "Pennsylvania";
                    }
                    return s;
                });

                var data = Processor.GetObjects<BankRecord>(parser, mapper, preProcessor);
                int index = 0;
                foreach (var record in data.Where(d => d.State.Equals("Pennsylvania")))
                {
                    var computed = $"Bank {record.Name} from {record.State} closed {record.ClosingDate}";
                    Assert.AreEqual(expected[index++], computed);
                }
            }
        }

        [Test]
        public void Documentation_PostProcessor()
        {
            var expected = new string[] {
                "Bank First CornerStone Bank from PA closed 5/6/2016 12:00:00 AM",
                "Bank Vantage Point Bank from PA closed 2/28/2014 12:00:00 AM",
                "Bank NOVA Bank from PA closed 10/26/2012 12:00:00 AM",
                "Bank American Eagle Savings Bank from PA closed 1/20/2012 12:00:00 AM",
                "Bank Public Savings Bank from PA closed 8/18/2011 12:00:00 AM",
                "Bank ARCHIVED! from PA closed 12/10/2010 12:00:00 AM",
                "Bank ARCHIVED! from PA closed 11/19/2010 12:00:00 AM",
                "Bank ARCHIVED! from PA closed 8/14/2009 12:00:00 AM",
                "Bank ARCHIVED! from PA closed 2/2/2007 12:00:00 AM",
                "Bank ARCHIVED! from PA closed 11/14/2003 12:00:00 AM"
            };
            using (var stream = _banklist)
            {
                var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote);
                var postProcessor = new PostProcessor<BankRecord>();

                postProcessor.AddFunction((bank) =>
                {
                    if (bank.ClosingDate.Year <= 2010)
                    {
                        bank.Name = "ARCHIVED!";
                    }
                    return bank;
                });

                var mapper = new TextMapper(parser.Header);
                var data = Processor.GetObjects<BankRecord>(parser, mapper, null, postProcessor);

                int index = 0;
                foreach (var record in data.Where(d => d.State.Equals("PA")))
                {
                    var computed = $"Bank {record.Name} from {record.State} closed {record.ClosingDate}";
                    Assert.AreEqual(expected[index++], computed);
                }
            }
        }
        
    }   
}
