﻿using NUnit.Framework;
using NFile.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFile.Testing
{
    [TestFixture]
    public class i_Processor_dynamic
    {
        [Test]
        public void Process_Dynamic()
        {

            var file = ResourceHelper.GetResource("TestFilePipe.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void Process_Dynamic_Pipe_Nulls()
        {
            var file = ResourceHelper.GetResource("TestFilePipeEmptyCells.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("", objects[0].ColumnA);
            Assert.AreEqual("Data1B", objects[0].ColumnB);
            Assert.AreEqual("Data1C", objects[0].ColumnC);
            Assert.AreEqual("Data1D", objects[0].ColumnD);
            Assert.AreEqual("Data2A", objects[1].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("", objects[1].ColumnC);
            Assert.AreEqual("Data2D", objects[1].ColumnD);
        }

        [Test]
        public void Process_Dynamic_Pipe_Qualified_Nulls()
        {
            var file = ResourceHelper.GetResource("TestFilePipeDoubleQuoteEmptyCells.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.DoubleQuote);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("", objects[0].ColumnA);
            Assert.AreEqual("Data1B", objects[0].ColumnB);
            Assert.AreEqual("Data1C", objects[0].ColumnC);
            Assert.AreEqual("Data1D", objects[0].ColumnD);
            Assert.AreEqual("Data2A", objects[1].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("", objects[1].ColumnC);
            Assert.AreEqual("Data2D", objects[1].ColumnD);
        }

        [Test]
        public void Process_Dynamic_Pipe_ColumnMismatch()
        {
            var file = ResourceHelper.GetResource("TestFilePipeColumnMismatch.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data1B", objects[0].ColumnB);
            Assert.AreEqual("Data1C", objects[0].ColumnC);
            Assert.AreEqual("Data1D", objects[0].ColumnD);
            Assert.AreEqual("Data2A", objects[1].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual(null, objects[1].ColumnC);
            Assert.AreEqual(null, objects[1].ColumnD);
            Assert.AreEqual("Data3A", objects[2].ColumnA);
            Assert.AreEqual("Data3B", objects[2].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual(null, objects[2].ColumnD);
            Assert.AreEqual(null, objects[4].ColumnB);
            Assert.AreEqual(null, objects[4].ColumnC);
            Assert.AreEqual(null, objects[4].ColumnD);
        }

        [Test]
        public void Process_Dynamic_Header_Spaces()
        {

            var file = ResourceHelper.GetResource("TypedTestFilePipeHeaderSpaces.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(1, objects.Count);
            Assert.AreEqual("15", objects[0]["Total Number"]);
            Assert.AreEqual("2016-08-29", objects[0]["Date Qualified"]);
            Assert.AreEqual("True", objects[0]["Active Bit"]);
            Assert.AreEqual("noone@somewhere.com", objects[0]["Email Address"]);
            Assert.AreEqual("Extra Data", objects[0].Meta1);
            Assert.AreEqual("$19.95", objects[0]["Total Processed $ (YTD)"]);
        }

        [Test]
        public void Process_Dynamic_Pipe_ColumnDoesntExist_Property()
        {
            var file = ResourceHelper.GetResource("TestFilePipeColumnMismatch.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.Throws<ArgumentException>(() => objects[0].NonExistingProperty.ToString());
            
        }

        [Test]
        public void Process_Dynamic_Pipe_ColumnDoesntExist_Indexed()
        {
            var file = ResourceHelper.GetResource("TestFilePipeColumnMismatch.dat");
            var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
            var mapper = new TextMapper(parser.Header);
            var objects = Processor.GetDynamicObjects(parser, mapper).ToList();

            Assert.AreEqual(5, objects.Count);
            Assert.Throws<ArgumentException>(() => objects[0]["NonExistingProperty"].ToString() );
        }
    }
}
