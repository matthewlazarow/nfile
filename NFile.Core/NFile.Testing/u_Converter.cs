﻿using System;
using NUnit.Framework;
using System.Text;
using System.Collections.Generic;
using NFile.Testing.TestFixturees;
using System.Linq;
using NFile.Core;

namespace NFile.Testing
{
    [TestFixture]
    public class u_Converter
    {
        [Test]
        public void TextConverter_Convert_Stream()
        {
            var objects = new List<TestFileClass>();
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var converter = new TextConverter(parser, mapper);
                objects =  converter.Convert<TestFileClass>().ToList();
            }

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }

        [Test]
        public void TextConverter_Incorrect_Mappings()
        {
            var objects = new List<TestFileClass>();
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper()
                    .AddMapping("NotThere", 0)
                    .AddMapping("SomethingElse", 1)
                    .AddMapping("Missing!", 2);

                var converter = new TextConverter(parser, mapper);
                objects = converter.Convert<TestFileClass>().ToList();
            }

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual(null, objects[0].ColumnA);
            Assert.AreEqual(null, objects[1].ColumnB);
        }

        [Test]
        public void TextConverter_Partial_Mappings()
        {
            var objects = new List<TestFileClass>();
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper()
                    .AddMapping("ColumnA", 0)
                    .AddMapping("ColumnB", 1);

                var converter = new TextConverter(parser, mapper);
                objects = converter.Convert<TestFileClass>().ToList();
            }

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual(null, objects[2].ColumnC);
            Assert.AreEqual(null, objects[3].ColumnD);
        }

        [Test]
        public void TextConverter_Extra_Mappings()
        {
            var objects = new List<TestFileClass>();
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper()
                    .AddMapping("ColumnA", 0)
                    .AddMapping("ColumnB", 1)
                    .AddMapping("ColumnC", 2)
                    .AddMapping("ColumnD", 3)
                    .AddMapping("ColumnE", 4)
                    .AddMapping("ColumnF", 5)
                    .AddMapping("ColumnG", 6);

                var converter = new TextConverter(parser, mapper);
                objects = converter.Convert<TestFileClass>().ToList();
            }

            Assert.AreEqual(5, objects.Count);
            Assert.AreEqual("Data1A", objects[0].ColumnA);
            Assert.AreEqual("Data2B", objects[1].ColumnB);
            Assert.AreEqual("Data3C", objects[2].ColumnC);
            Assert.AreEqual("Data4D", objects[3].ColumnD);
            Assert.AreEqual("Data5A", objects[4].ColumnA);
        }
    }
}
