﻿using System;
using NUnit.Framework;
using NFile.Core;

namespace NFile.Testing
{
    [TestFixture]
    public class u_Mapper
    {
        [Test]
        public void Map_MultiColumn_Duplicate_Columns_Exception()
        {
            var header = new[] { "ColumnA", "ColumnA", "ColumnB", "ColumnC" };
            Assert.Throws<ArgumentException>(() => new TextMapper(header));           
        }

        [Test]
        public void Map_MultiColumn_ByMethod()
        {
            var mapper = new TextMapper()
                .AddMapping("ColumnA", 0)
                .AddMapping("ColumnB", 1)
                .AddMapping("ColumnC", 2)
                .AddMapping("ColumnD", 3);

            Assert.AreEqual("ColumnA", mapper.FindColumnName(0));
            Assert.AreEqual("ColumnB", mapper.FindColumnName(1));
            Assert.AreEqual("ColumnC", mapper.FindColumnName(2));
            Assert.AreEqual("ColumnD", mapper.FindColumnName(3));
        }

        [Test]
        public void Map_AddMapping_Index_Exception()
        {
            Assert.Throws<ArgumentException>(() => 
            {
                new TextMapper()
                .AddMapping("ColumnA", 0)
                .AddMapping("ColumnB", 1)
                .AddMapping("ColumnC", 1);
            });            
        }

        [Test]
        public void Map_AddMapping_Name_Exception()
        {
            Assert.Throws<ArgumentException>(() => 
            {
                new TextMapper()
                .AddMapping("ColumnA", 0)
                .AddMapping("ColumnB", 1)
                .AddMapping("ColumnB", 2);
             });
        }

        [Test]
        public void Map_MultiColumn_FindByIndex()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            Assert.AreEqual("ColumnA", mapper.FindColumnName(0));
            Assert.AreEqual("ColumnB", mapper.FindColumnName(1));
            Assert.AreEqual("ColumnC", mapper.FindColumnName(2));
            Assert.AreEqual("ColumnD", mapper.FindColumnName(3));
        }

        [Test]
        public void Map_MultiColumn_FindByName_Default()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            Assert.AreEqual(0, mapper.FindColumnIndex("ColumnA"));
            Assert.AreEqual(1, mapper.FindColumnIndex("ColumnB"));
            Assert.AreEqual(2, mapper.FindColumnIndex("ColumnC"));
            Assert.AreEqual(3, mapper.FindColumnIndex("ColumnD"));
        }

        [Test]
        public void Map_MultiColumn_FindByName_Spaces()
        {
            var header = new[] { "Column A", "Column B", "Column (C)", "Column _($D)" };

            var mapper = new TextMapper(header);

            Assert.AreEqual(0, mapper.FindColumnIndex("Column A"));
            Assert.AreEqual(1, mapper.FindColumnIndex("Column B"));
            Assert.AreEqual(2, mapper.FindColumnIndex("Column (C)"));
            Assert.AreEqual(3, mapper.FindColumnIndex("Column _($D)"));
        }

        [Test]
        public void Map_MultiColumn_FindByName_Empty_Single()
        {
            var header = new[] { "ColumnA", "", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            Assert.AreEqual(0, mapper.FindColumnIndex("ColumnA"));
            Assert.AreEqual(1, mapper.FindColumnIndex(""));
            Assert.AreEqual(2, mapper.FindColumnIndex("ColumnC"));
            Assert.AreEqual(3, mapper.FindColumnIndex("ColumnD"));
        }

        [Test]
        public void Map_GetMapping()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            var mappingA = mapper.GetMapping("ColumnA");
            var mappingB = mapper.GetMapping("ColumnB");
            var mappingC = mapper.GetMapping("ColumnC");
            var mappingD = mapper.GetMapping("ColumnD");

            Assert.AreEqual(0, mappingA.ColumnIndex);
            Assert.AreEqual("ColumnA", mappingA.ColumnName);
            Assert.AreEqual(1, mappingB.ColumnIndex);
            Assert.AreEqual("ColumnB", mappingB.ColumnName);
            Assert.AreEqual(2, mappingC.ColumnIndex);
            Assert.AreEqual("ColumnC", mappingC.ColumnName);
            Assert.AreEqual(3, mappingD.ColumnIndex);
            Assert.AreEqual("ColumnD", mappingD.ColumnName);
        }

        [Test]
        public void Map_GetMapping_Indexer_Name()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            var mappingA = mapper["ColumnA"];
            var mappingB = mapper["ColumnB"];
            var mappingC = mapper["ColumnC"];
            var mappingD = mapper["ColumnD"];

            Assert.AreEqual(0, mappingA.ColumnIndex);
            Assert.AreEqual("ColumnA", mappingA.ColumnName);
            Assert.AreEqual(1, mappingB.ColumnIndex);
            Assert.AreEqual("ColumnB", mappingB.ColumnName);
            Assert.AreEqual(2, mappingC.ColumnIndex);
            Assert.AreEqual("ColumnC", mappingC.ColumnName);
            Assert.AreEqual(3, mappingD.ColumnIndex);
            Assert.AreEqual("ColumnD", mappingD.ColumnName);
        }

        [Test]
        public void Map_GetMapping_Indexer_Index()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            var mappingA = mapper[0];
            var mappingB = mapper[1];
            var mappingC = mapper[2];
            var mappingD = mapper[3];

            Assert.AreEqual(0, mappingA.ColumnIndex);
            Assert.AreEqual("ColumnA", mappingA.ColumnName);
            Assert.AreEqual(1, mappingB.ColumnIndex);
            Assert.AreEqual("ColumnB", mappingB.ColumnName);
            Assert.AreEqual(2, mappingC.ColumnIndex);
            Assert.AreEqual("ColumnC", mappingC.ColumnName);
            Assert.AreEqual(3, mappingD.ColumnIndex);
            Assert.AreEqual("ColumnD", mappingD.ColumnName);
        }

        [Test]
        public void Map_MultiColumn_FindByName_Ignore_Case()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };

            var mapper = new TextMapper(header);

            Assert.AreEqual(0, mapper.FindColumnIndex("columna", StringComparison.CurrentCultureIgnoreCase));
            Assert.AreEqual(1, mapper.FindColumnIndex("columnb", StringComparison.CurrentCultureIgnoreCase));
            Assert.AreEqual(2, mapper.FindColumnIndex("columnc", StringComparison.CurrentCultureIgnoreCase));
            Assert.AreEqual(3, mapper.FindColumnIndex("columnd", StringComparison.CurrentCultureIgnoreCase));
        }

        [Test]
        public void Map_MultiColumn_FindByName_DoesntExist()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };
            var mapper = new TextMapper(header);

            var result = mapper.FindColumnIndex("Tacos");

            Assert.IsNull(result);
        }

        [Test]
        public void Map_GetMapping_DoesntExist()
        {
            var header = new[] { "ColumnA", "ColumnB", "ColumnC", "ColumnD" };
            var mapper = new TextMapper(header);

            var result = mapper.GetMapping("Tacos");

            Assert.IsNull(result);
        }
    }
}
