﻿using System;
using NUnit.Framework;
using System.IO;
using System.Text;
using NFile.Core;
using System.Diagnostics;
using NFile.Core.Output;
using System.Collections.Generic;
using NFile.Testing.TestFixturees;

namespace NFile.Testing
{
    [TestFixture]
    public class u_Output
    {
       [Test]
       public void Output_Text_InMemory_Pipe()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = 5,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                },
                new
                {
                    ColumnA = "Data2A",
                    ColumnB = 10,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual(results, "\"ColumnA\"|\"ColumnB\"|\"ColumnC\"\r\n\"Data1A\"|\"5\"|\"8/6/2016 7:30:41 AM\"\r\n\"Data2A\"|\"10\"|\"8/6/2016 7:30:41 AM\"\r\n");
          
        }

        [Test]
        public void Output_Text_InMemory_Pipe_No_Headers()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = 5,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                },
                new
                {
                    ColumnA = "Data2A",
                    ColumnB = 10,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, null, false), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual("\"Data1A\"|\"5\"|\"8/6/2016 7:30:41 AM\"\r\n\"Data2A\"|\"10\"|\"8/6/2016 7:30:41 AM\"\r\n", results);
        }
                
        [Test]
        public void Output_Text_HiddenAttribute()
        {
            var data = new TypedTestFixtureAttributes[] {
                new TypedTestFixtureAttributes {
                    BoolColumn = true,
                    StringColumn = "Test1",
                    Hidden = "Hidden1"
                },
                new TypedTestFixtureAttributes {
                    BoolColumn = false,
                    StringColumn = "Test2",
                    Hidden = "Hidden2"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.None, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            var expected = "IntColumn|DateColumn|Is Currently Active|Email Address|Total ' and \"" + Environment.NewLine +
                           "0||True|Test1|" + Environment.NewLine +
                           "0||False|Test2|" + Environment.NewLine;

            Assert.AreEqual(expected, results);

        }
        
        [Test]
        public void Output_Text_Custom_Header_Name_Attribute()
        {
            var data = new TypedTestFixtureAttributes[] {
                new TypedTestFixtureAttributes {
                    BoolColumn = true,
                    StringColumn = "Test1",
                    Hidden = "Hidden1"
                },
                new TypedTestFixtureAttributes {
                    BoolColumn = false,
                    StringColumn = "Test2",
                    Hidden = "Hidden2"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.None, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            var expected = "IntColumn|DateColumn|Is Currently Active|Email Address|Total ' and \"" + Environment.NewLine +
                           "0||True|Test1|" + Environment.NewLine +
                           "0||False|Test2|" + Environment.NewLine;

            Assert.AreEqual(expected, results);

        }

        [Test]
        public void Output_Text_Custom_Header_Name_Attribute_Text_Qualified_And_Escaped()
        {
            var data = new TypedTestFixtureAttributes[] {
                new TypedTestFixtureAttributes {
                    BoolColumn = true,
                    StringColumn = "Test1",
                    Hidden = "Hidden1"
                },
                new TypedTestFixtureAttributes {
                    BoolColumn = false,
                    StringColumn = "Test2",
                    Hidden = "Hidden2"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Comma, TextQualifier.Quote, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            var expected = "'IntColumn','DateColumn','Is Currently Active','Email Address','Total '' and \"'" + Environment.NewLine +
                           "'0','','True','Test1',''" + Environment.NewLine +
                           "'0','','False','Test2',''" + Environment.NewLine;

            Assert.AreEqual(expected, results);

        }

        [Test]
        public void Output_Text_InMemory_Pipe_Escape_Qualifier_Both_SingleQuote()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A with no quotes",
                    ColumnB = "1' 3\" is where it's at",
                    ColumnC = "sometimes ya'll need some it's good will provided by billy's baseball"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.Quote, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual("'ColumnA'|'ColumnB'|'ColumnC'\r\n\'Data1A with no quotes'|'1'' 3\" is where it''s at'|'sometimes ya''ll need some it''s good will provided by billy''s baseball'\r\n", results);

        }

        [Test]
        public void Output_Text_InMemory_Pipe_Escape_Qualifier_Both_DoubleQuote()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A with no quotes",
                    ColumnB = "1' 3\" is where it's at",
                    ColumnC = "\"some day we will figure it out\" sasha said with a grin"                    
                }                
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"\r\n\"Data1A with no quotes\"|\"1' 3\"\" is where it's at\"|\"\"\"some day we will figure it out\"\" sasha said with a grin\"\r\n", results);

        }

        [Test]
        public void Output_Text_InMemory_Pipe__NullValue()
        {
            string badvalue = null;
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = badvalue,
                    ColumnC = "Data3A"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.None, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual("ColumnA|ColumnB|ColumnC\r\nData1A||Data3A\r\n", results);

        }

        [Test]
        public void Output_Text_InMemory_Pipe_Escape_Qualifier_NullValue()
        {
            string badvalue = null;
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = badvalue,
                    ColumnC = "Data3A"
                }
            };

            var memory = new MemoryStream();
            var export = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, null), () => memory);
            export.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(memory.ToArray());

            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"\r\n\"Data1A\"|\"\"|\"Data3A\"\r\n", results);
        }

        [Test]
        public void Output_Text_Simple_AnonymousNestedObjects_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = 5,
                    ColumnC = new { Nested1 = "Some nested data for 1C", Nested2 = "Some more nested data for 1C" }
                },
                new
                {
                    ColumnA = "Data2A",
                    ColumnB = 10,
                    ColumnC = new { Nested1 = "Some nested data for 2C", Nested2 = "Some more nested data for 2C" }
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"Data1A\"|\"5\"|\"<object>" + Environment.NewLine +
                            "  <Nested1>Some nested data for 1C</Nested1>" + Environment.NewLine +
                            "  <Nested2>Some more nested data for 1C</Nested2>" + Environment.NewLine +
                            "</object>\"" + Environment.NewLine +
                            "\"Data2A\"|\"10\"|\"<object>" + Environment.NewLine +
                            "  <Nested1>Some nested data for 2C</Nested1>" + Environment.NewLine +
                            "  <Nested2>Some more nested data for 2C</Nested2>" + Environment.NewLine +
                            "</object>\"" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_CustomOutput()
        {
            var data = new[] {
                new
                {
                    ColumnA = "Data1A",
                    ColumnB = 5,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                },
                new
                {
                    ColumnA = "Data2A",
                    ColumnB = 10,
                    ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.None, null), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual(@"ColumnA|ColumnB|ColumnC" + Environment.NewLine +
                            "Data1A|5|8/6/2016 7:30:41 AM" + Environment.NewLine +
                            "Data2A|10|8/6/2016 7:30:41 AM" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_Save_Single_Object_Test_CustomOutput()
        {
            var data = new
            {
                ColumnA = "Data1A",
                ColumnB = 5,
                ColumnC = DateTime.Parse("8/6/2016 7:30:41 AM")
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.None, null), () => output);
            text.Save(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual(@"ColumnA|ColumnB|ColumnC" + Environment.NewLine +
                            "Data1A|5|8/6/2016 7:30:41 AM" + Environment.NewLine, results);

        }

        [Test]
        public void Output_Text_Simple_Array_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "FlatColumnA",
                    ColumnB = new string[]
                    {
                        "ColumnB_1", "ColumnB_2", "ColumnB_3"
                    },
                    ColumnC = "FlatColumnC"
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"FlatColumnA\"|\"<object>" + Environment.NewLine +
                            "  <objectItem>ColumnB_1</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_2</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_3</objectItem>" + Environment.NewLine +
                            "</object>\"|\"FlatColumnC\"" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_Simple_List_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "FlatColumnA",
                    ColumnB = new List<string>
                    {
                        "ColumnB_1", "ColumnB_2", "ColumnB_3"
                    },
                    ColumnC = "FlatColumnC"
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"FlatColumnA\"|\"<object>" + Environment.NewLine +
                            "  <objectItem>ColumnB_1</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_2</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_3</objectItem>" + Environment.NewLine +
                            "</object>\"|\"FlatColumnC\"" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_Simple_Enumerable_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "FlatColumnA",
                    ColumnB = new List<string>
                    {
                        "ColumnB_1", "ColumnB_2", "ColumnB_3"
                    },
                    ColumnC = "FlatColumnC"
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"FlatColumnA\"|\"<object>" + Environment.NewLine +
                            "  <objectItem>ColumnB_1</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_2</objectItem>" + Environment.NewLine +
                            "  <objectItem>ColumnB_3</objectItem>" + Environment.NewLine +
                            "</object>\"|\"FlatColumnC\"" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_Simple_AnonymousNestedObjects_Deep_Serialize_XML()
        {
            var data = new
            {
                ColumnA = "Data1A",
                ColumnB = 5,
                ColumnC = new
                {
                    Nested1 = "Some nested data for 1C",
                    Nested2 = new
                    {
                        Nested3 = "2 levels deep",
                        Nested4 = new
                        {
                            Nested5 = "3 levels deep"
                        }
                    }
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.Save(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual(
                "\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                "\"Data1A\"|\"5\"|\"<object>" + Environment.NewLine +
                "  <Nested1>Some nested data for 1C</Nested1>" + Environment.NewLine +
                "  <Nested2>" + Environment.NewLine +
                "    <Nested3>2 levels deep</Nested3>" + Environment.NewLine +
                "    <Nested4>" + Environment.NewLine +
                "      <Nested5>3 levels deep</Nested5>" + Environment.NewLine +
                "    </Nested4>" + Environment.NewLine +
                "  </Nested2>" + Environment.NewLine +
                "</object>\"" + Environment.NewLine, results);
        }

        [Test]
        public void Output_Text_Simple_ConcreteNestedObjects_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "FlatColumnA",
                    ColumnB = new TestFixturees.TypedTestFixture
                    {
                        StringColumn = "Some String Data",
                        BoolColumn = true,
                        DateColumn = DateTime.Parse("2016-09-26"),
                        IntColumn = 15
                    },
                    ColumnC = "FlatColumnC"
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"FlatColumnA\"|\"<object>" + Environment.NewLine +
                            "  <IntColumn>15</IntColumn>" + Environment.NewLine +
                            "  <DateColumn>2016-09-26T00:00:00</DateColumn>" + Environment.NewLine +
                            "  <BoolColumn>true</BoolColumn>" + Environment.NewLine +
                            "  <StringColumn>Some String Data</StringColumn>" + Environment.NewLine +
                            "</object>\"|\"FlatColumnC\"" + Environment.NewLine, results);
        }
                
        [Test]
        public void Output_Text_Simple_ConcreteNestedObjects_Array_Serialize_XML()
        {
            var data = new[] {
                new
                {
                    ColumnA = "FlatColumnA",
                    ColumnB = new TestFixturees.TypedTestFixture[]
                    {
                        new TestFixturees.TypedTestFixture
                        {
                            StringColumn = "Some String Data",
                            BoolColumn = true,
                            DateColumn = DateTime.Parse("2016-09-26"),
                            IntColumn = 15
                        },
                        new TestFixturees.TypedTestFixture
                        {
                            StringColumn = "Some String Data",
                            BoolColumn = true,
                            DateColumn = DateTime.Parse("2016-09-26"),
                            IntColumn = 15
                        },
                    },
                    ColumnC = "FlatColumnC"
                }
            };

            var output = new MemoryStream();
            var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);
            text.SaveMany(data);
            var results = ASCIIEncoding.ASCII.GetString(output.ToArray());
            Assert.AreEqual("\"ColumnA\"|\"ColumnB\"|\"ColumnC\"" + Environment.NewLine +
                            "\"FlatColumnA\"|\"<object>" + Environment.NewLine +
                            "  <objectItem>" + Environment.NewLine +
                            "    <IntColumn>15</IntColumn>" + Environment.NewLine +
                            "    <DateColumn>2016-09-26T00:00:00</DateColumn>" + Environment.NewLine +
                            "    <BoolColumn>true</BoolColumn>" + Environment.NewLine +
                            "    <StringColumn>Some String Data</StringColumn>" + Environment.NewLine +
                            "  </objectItem>" + Environment.NewLine +
                            "  <objectItem>" + Environment.NewLine +
                            "    <IntColumn>15</IntColumn>" + Environment.NewLine +
                            "    <DateColumn>2016-09-26T00:00:00</DateColumn>" + Environment.NewLine +
                            "    <BoolColumn>true</BoolColumn>" + Environment.NewLine +
                            "    <StringColumn>Some String Data</StringColumn>" + Environment.NewLine +
                            "  </objectItem>" + Environment.NewLine +
                            "</object>\"|\"FlatColumnC\"" + Environment.NewLine, results);
        }
    }
}
