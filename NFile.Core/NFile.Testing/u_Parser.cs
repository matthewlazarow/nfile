﻿using System;
using NUnit.Framework;
using System.IO;
using System.Linq;
using System.Reflection;
using NFile.Core;

namespace NFile.Testing
{
    [TestFixture]
    public class u_Parser
    {
        [Test]
        public void Parse_Single_Line_Pipe()
        {            
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRow();

                Assert.IsTrue(result.Length == 4);
                Assert.AreEqual(result[0], "Data1A");
                Assert.AreEqual(result[1], "Data1B");
                Assert.AreEqual(result[2], "Data1C");
                Assert.AreEqual(result[3], "Data1D");
            }
        }

        [Test]
        public void Parse_ReadRows_Pipe()
        {
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRows().ToArray();

                Assert.IsTrue(result.Count() == 5);
                Assert.AreEqual(result.First()[0], "Data1A");
                Assert.AreEqual(result.First()[1], "Data1B");
                Assert.AreEqual(result.First()[2], "Data1C");
                Assert.AreEqual(result.First()[3], "Data1D");
                Assert.AreEqual(result.Last()[3], "Data5D");
            }
        }

        [Test]
        public void Parse_Pipe_Verify_Mapping()
        {
            using (var file = ResourceHelper.GetResource("TestFilePipe.dat"))
            {
                var parser = new TextParser(file, Delimiter.Pipe, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);

                Assert.AreEqual(0, mapper.FindColumnIndex("ColumnA"));
                Assert.AreEqual(1, mapper.FindColumnIndex("ColumnB"));
                Assert.AreEqual(2, mapper.FindColumnIndex("ColumnC"));
                Assert.AreEqual(3, mapper.FindColumnIndex("ColumnD"));
            }
        }

        [Test]
        public void Parse_Single_Line_Comma()
        {
            using (var file = ResourceHelper.GetResource("TestFileComma.csv"))
            {
                var parser = new TextParser(file, Delimiter.Comma, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRow();

                Assert.IsTrue(result.Length == 4);
                Assert.AreEqual("Data1A", result[0]);
                Assert.AreEqual("Data1B", result[1]);
                Assert.AreEqual("Data1C", result[2]);
                Assert.AreEqual("Data1D", result[3]);
            }
        }

        [Test]
        public void Parse_Single_Line_Text_Qualified()
        {
            var data = "Column1,Column2,Column3\n" +
                       "Data1,\"Data2, qualified\",Data3";

            using (var stream = GenerateStreamFromString(data))
            {
                var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRow();

                Assert.IsTrue(result.Length == 3);
                Assert.AreEqual("Data1", result[0]);
                Assert.AreEqual("Data2, qualified", result[1]);
                Assert.AreEqual("Data3", result[2]);
            }
        }

        [Test]
        public void Parse_Single_Line_Tab()
        {
            var data = "Column1\tColumn2\tColumn3\n" +
                       "Data1\tData2\tData3";

            using (var stream = GenerateStreamFromString(data))
            {
                var parser = new TextParser(stream, Delimiter.Tab, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRow();

                Assert.IsTrue(result.Length == 3);
                Assert.AreEqual("Data1", result[0]);
                Assert.AreEqual("Data2", result[1]);
                Assert.AreEqual("Data3", result[2]);
            }
        }

        [Test]
        public void Parse_ReadRows_Tab()
        {
            using (var file = ResourceHelper.GetResource("TestFileTab.dat"))
            {
                var parser = new TextParser(file, Delimiter.Tab, TextQualifier.None);
                var mapper = new TextMapper(parser.Header);
                var result = parser.ReadRows().ToArray();

                Assert.IsTrue(result.Count() == 5);
                Assert.AreEqual(result.First()[0], "Data1A");
                Assert.AreEqual(result.First()[1], "Data1B");
                Assert.AreEqual(result.First()[2], "Data1C");
                Assert.AreEqual(result.First()[3], "Data1D");
                Assert.AreEqual(result.Last()[3], "Data5D");
            }
        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
