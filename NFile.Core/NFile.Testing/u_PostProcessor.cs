﻿using System;
using NUnit.Framework;
using NFile.Testing.TestFixturees;
using NFile.Core.PostProcessing;

namespace NFile.Testing
{
    [TestFixture]
    public class u_PostProcessor
    {
        [Test]
        public void PostProcessor_Set_All_PostProcessed()
        {
            var instance = new TestFileClass
            {
                ColumnA = "Data field 1",
                ColumnB = "Data field 2",
                ColumnC = "Data field 3",
                ColumnD = "Data field 4"
            };

            var postProcessor = new PostProcessor<TestFileClass>();

            var token = "HelloWorld!";
            postProcessor.AddFunction((test) =>
            {
                test.ColumnA = token;
                test.ColumnB = token;
                test.ColumnC = token;
                test.ColumnD = token;

                return test;
            });

            var results = postProcessor.Process(instance);

            Assert.AreEqual(token, results.ColumnA);
            Assert.AreEqual(token, results.ColumnB);
            Assert.AreEqual(token, results.ColumnC);
            Assert.AreEqual(token, results.ColumnD);

        }

        [Test]
        public void PostProcessor_ConditionalLogic()
        {
            var instance1 = new TypedTestFixture
            {
                BoolColumn = true,
                StringColumn = "Hello, World"                
            };

            var instance2 = new TypedTestFixture
            {
                BoolColumn = false,
                StringColumn = "Hello, World",              
            };

            var postProcessor = new PostProcessor<TypedTestFixture>();
                        
            postProcessor.AddFunction((test) =>
            {
                if (test.BoolColumn)
                {
                    test.StringColumn = "BoolColumnSet";
                }
                return test;
            });

            var result1 = postProcessor.Process(instance1);
            var result2 = postProcessor.Process(instance2);

            Assert.AreEqual(true, result1.BoolColumn);
            Assert.AreEqual("BoolColumnSet", result1.StringColumn);

            Assert.AreEqual(false, result2.BoolColumn);
            Assert.AreEqual("Hello, World", result2.StringColumn);
        }
    }
}
