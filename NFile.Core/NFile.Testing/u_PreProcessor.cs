﻿using System;
using NUnit.Framework;
using NFile.Core;
using NFile.Core.PreProcessing;

namespace NFile.Testing
{
    [TestFixture]
    public class u_PreProcessor
    {
        [Test]
        public void Prepend_All_With_HelloWorld()
        {
            var cols = new string[] { "Col1", "Col2", "Col3" };
            var mapping = new TextMapper(cols);
            var datarow = new string[] { "Value1", "Value2", "Value3" };

            var preProcessor = new TextPreProcessor(mapping);

            var token = "HelloWorld!";
            foreach(var col in cols)
                preProcessor.AddFunction(col, (s) => $"{token} {s}");

            var results = preProcessor.Process(datarow);

            Assert.AreEqual($"{token} Value1", results[0]);
            Assert.AreEqual($"{token} Value2", results[1]);
            Assert.AreEqual($"{token} Value3", results[2]);
        }
    }
}
