# NFile Readme #
***

### What is NFile?
NFile is designed to be a quick and easy way to deserialize data into objects, provide a framework for manipulation, to serialize the objects back out to some output module.

### Who do I talk to? ###
* matthew.lazarow at gmail.com - Questions, comments, support, feature requests

### How to use NFile

Use the [ Nuget package ](https://www.nuget.org/packages/NFile.Core/) or build the source. Add **Nfile.Core.dll** as a project reference.
NFile is designed to be flexible, but for most simple IO tasks, **NFile.Core.Processor** has the following helper methods:

**GetObjects<T>** - Convert the file into an IEnumerable collection of the supplied class. Column headers are mapped up automatically if a matching public property is found in the class.

**GetDyamicObjects** - Convert the file into an IEnumerable collection of dynamic objects.

**SaveObjects** - Given a collection of objects, saves them out to a file. This is great for quick and dirty dumps of information.

* * *
For the following examples, I will use the *FDIC Failed Bank List* provided by [Data.gov](https://catalog.data.gov/dataset/fdic-failed-bank-list).
**example**


Bank Name                    | City            | ST | CERT  | Acquiring Institution               | Closing Date | Updated Date
---------------------------- | --------------- | -- | ----- | ----------------------------------- | ------------ | ------------
Allied Bank	                 | Mulberry        | AR | 91    | Today's Bank                        | 23-Sep-16    | 17-Oct-16
The Woodbury Banking Company | Woodbury        | GA | 11297 | United Bank                         | 19-Aug-16    | 17-Oct-16
First CornerStone Bank       | King of Prussia | PA | 35312 | First-Citizens Bank & Trust Company | 6-May-16     | 6-Sep-16

* * *
### Reading Data

#### Converting a file to dynamic type

The quickest way to get at your data is the `GetDynamicObjects` method. This will process the file and convert the column headers found in the first row to dynamic values. Empty cells are blank, and missing cells (header mismatch) are converted to null.

```
#!C#

var data = Processor.GetDynamicObjects("E:/datasets/banklist.csv", Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default);

//note how simple column headers without spaces can be accessed by dot property notation, or by name via the indexer.
foreach (var record in data.Where(d => d.ST == "PA"))  
{
	Console.WriteLine($"Bank {record["Bank Name"]} from {record["ST"]} closed {record["Closing Date"]}");
}
```

* * *

#### Converting a file to objects

We also can gain further control by providing a data contract based on the file. Using our banklist file, a contract may look something like this:

```
#!C#
public class BankRecord
{
	public Guid Id { get; set; }

	[ColumnHeader("Bank Name")]
	public string Name { get; set; }
	
	public string City { get; set; }
	
	[ColumnHeader("ST")]
    public string State { get; set; }
	
	public string CERT { get; set; }
	
	[ColumnHeader("Acquiring Institution")]
	public string Buyer { get; set; }
	
	[ColumnHeader("Closing Date")]
    public DateTime ClosingDate { get; set; }
	
	[ColumnHeader("Updated Date")]
	public DateTime Updated { get; set;}
}
```
The processor will try to match a column header first by the value supplied by the ColumnHeader attribute (using Nfile.Core.Attributes). If one is not supplied, 
it will then try to match based on an identical property value (like City and CERT above). Any other columns found will be ignored. With our data contract in place, we can 
use GetObjects<T>.


```
#!C#

var data = Processor.GetObjects<BankRecord>("E:/datasets/banklist.csv", Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default);
	
	foreach (var record in data.Where(d => d.State == "PA"))
	{
		Console.WriteLine($"Bank {record.Name} from {record.State} closed {record.ClosingDate}");
	}

```

#### Manual Mappings and Headless Files
When using the helper methods, it is assumed the header is contained in the first row, However, that may not always be the case. It is possible
to manually generate the headers and to notify the parser that the source file does not contain a header.

```
#!C#
using (var stream = File.OpenRead("E:/datasets/contacts.csv"))
{
	var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote, Encoding.Default, false);
	var mapper = new TextMapper()
            .AddMapping("Email", 0)
            .AddMapping("First Name", 1)
            .AddMapping("Last Name", 2)
            .AddMapping("Phone", 3);
	var objects = Processor.GetObjects<Contacts>(parser, mapper).ToList();
}
```


* * *

#### Using a PreProcessor
If you find yourself in a situation where you need to modify the source value before the actual conversion takes 
place, you can create a class that inherits from Nfile.Core.PreProcessing.IPreProcessor.

```
#!C#

using (var stream = File.OpenRead("E:/datasets/banklist.csv"))
	{
		var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote);
		var mapper = new TextMapper(parser.Header);
        var preProcessor = new TextPreProcessor(mapper);

		preProcessor.AddFunction("ST", (s) =>
		{
			if (s.Equals("PA", StringComparison.CurrentCultureIgnoreCase))
			{
				return "Pennsylvania";
			}
			return s;
		});

		var data = Processor.GetObjects<BankRecord>(parser, mapper, preProcessor);

		foreach (var record in data.Where(d => d.State.Equals("Pennsylvania")))
		{
			Console.WriteLine($"Bank {record.Name} from {record.State} closed {record.ClosingDate}");
		}
	}

```

To use a pre-processor, it's a little more boilerplate, but handy when your hands are tied and the incoming data does not parse well. The pre-processor will hand you the raw string data, allowing you the chance to try parsing to whatever type you need.

```
#!C#
preProcessor.AddFunction("DateColumn", (s) =>
{                
    switch(s)
    {
        case "Yesterday" : return DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
        case "Tomorrow" : return DateTime.Now.AddDays(1.0);
        default: return DateTime.Parse(s);
    }
});

preProcessor.AddFunction("BoolColumn", (s) =>
{
    switch (s)
    {
        case "Nope": return false;
        case "Yep": return true;
        default: return bool.Parse(s);
    }
});
```

* * *

#### Using a PostProcessor
The PostProcessor will let you work with the entire data object immediately after it has been inflated. For example, 
in the following code, we are changing the Name property based on the value in the Closing Date column.

```
#!C#

using (var stream = File.OpenRead("E:/datasets/banklist.csv"))
	{
		var parser = new TextParser(stream, Delimiter.Comma, TextQualifier.DoubleQuote);
		var postProcessor = new PostProcessor<BankRecord>();

		postProcessor.AddFunction( (bank) =>
		{
		    if (bank.ClosingDate.Year <= 2010)
		    {
		        bank.Name = "ARCHIVED!";
			}
			return bank;
		});

		var mapper = new TextMapper(parser.Header);
        var data = Processor.GetObjects<BankRecord>(parser, mapper, null, postProcessor);

		foreach (var record in data.Where(d => d.State.Equals("PA")))
		{
			Console.WriteLine($"Bank {record.Name} from {record.State} closed {record.ClosingDate}");
		}
	}
```

* * *
### Saving Data

#### Saving objects to an IOutput
Exporting our data is a breeze! Using our example data set above, we can save out all of the banks that closed in PA like so:


```
#!C#

Processor.SaveObjects(data.Where(d => d.State.Equals("PA")), "E:/datasets/PABanks.csv", Delimiter.Comma, TextQualifier.DoubleQuote);

```
We also can pass any new object if we wanted to change the format while saving. 

```
#!C#
Processor.SaveObjects(
			data
				.Where(d => d.State.Equals("PA"))
				.Select(d => new { Bank = d.Name, Buyer = d.Buyer }),				
			"E:/datasets/PABanks.csv",
			Delimiter.Comma, 
			TextQualifier.DoubleQuote);
```

#### Customizing output with attributes
There may be some cases where you want to only output a few of your properties or use a different column header name to match another API. You can 
do this quite easily using attributes. Setting 'Save' to false prevents that property from being saved during output. If you need to change the name of the column 
header, set the 'HeaderName' value.

```
#!C#
public class BankRecord
{
    [OutputSave(Save = false)]
    public Guid Id { get; set; }

    [ColumnHeader("Bank Name")]
    [OutputSave(HeaderName = "Institution")]
    public string Name { get; set; }

    public string City { get; set; }

    [ColumnHeader("ST")]
    public string State { get; set; }

    public string CERT { get; set; }

    [ColumnHeader("Acquiring Institution")]
    [OutputSave(HeaderName = "Purchasing Bank")]
    public string Buyer { get; set; }

    [ColumnHeader("Closing Date")]
    [OutputSave(HeaderName = "Terminated Date")]
    public DateTime ClosingDate { get; set; }

    [ColumnHeader("Updated Date")]
    [OutputSave(HeaderName = "Modified")]
    public DateTime Updated { get; set; }
}
```

Saving the following yields:

Institution					 | City			   | State	| CERT  |Purchasing Bank                     | Terminated Date       | Modified
-----------------------------|-----------------|--------|-------|------------------------------------|-----------------------|-----------------------
Allied Banking				 | Mulberry		   | AR		| 91	|Today's Bank                        | 9/23/2016 12:00:00 AM | 10/17/2016 12:00:00 AM
The Woodbury Banking Company | Woodbury		   | GA		| 11297 |United Bank                         | 8/19/2016 12:00:00 AM | 10/17/2016 12:00:00 AM
First CornerStone Banking	 | King of Prussia | PA 	| 35312 |First-Citizens Bank & Trust Company | 5/6/2016 12:00:00 AM  | 9/6/2016 12:00:00 AM
.....


#### Serializing complex data types
When you have an Array, List, IEnumerable, or even a property that is another object itself, we need to know how to serialize the data when 
writing to the IOutput. Define how you wish for these objects to be serialized by implimenting an IComplexTypeOutputHandler to provide the IOutput object. 
Right now, the SimpleXMLOutputHandler is the default used for the Processor helper methods. You can create your own and pass it in using TextOutputSettings.


```
#!C#

var text = new TextOutput(new TextOutputSettings(Encoding.Default, Delimiter.Pipe, TextQualifier.DoubleQuote, new SimpleXMLOutputHandler()), () => output);

```

#### Escaping text qualifiers
When saving out to a file using text qualifiers, sometimes you need to save the qualifier value as part of the data creating the need to escape the qualifier. 
Single and double quotes are baked in, but if you need to use a custom qualifier with special needs, simply create a new TextQualifier


```
#!C#

var qualifier = new TextQualifier("\"", (s) => s.Replace("\"", "\"\""));

```

Or if you have different opening and closing qualifiers


```
#!C#

var qualifier = new TextQualifier("{", "}", (s) => s.Replace("{", "{{"), (s) => s.Replace("}", "}}");

```